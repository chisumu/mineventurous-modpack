#!/bin/sh
rm config.zip
cd minecraft
zip -r ../config.zip config/ patchouli_books
cd ..

MD5=`md5sum config.zip | cut -f 1 -d ' '`
sed -i.bak 's#<MD5>.*</MD5> <!-- auto:config.zip#<MD5>'${MD5}'</MD5> <!-- auto:config.zip#' mineventurous.xml

rm resources.zip
cd minecraft
zip -r resources.zip global_resource_packs/
cd ..

MD5=`md5sum resources.zip | cut -f 1 -d ' '`
sed -i.bak 's#<MD5>.*</MD5> <!-- auto:resources.zip#<MD5>'${MD5}'</MD5> <!-- auto:resources.zip#' mineventurous.xml

rm datapacks.zip
cd minecraft
zip -r datapacks.zip global_data_packs/
cd ..

MD5=`md5sum datapacks.zip | cut -f 1 -d ' '`
sed -i.bak 's#<MD5>.*</MD5> <!-- auto:datapacks.zip#<MD5>'${MD5}'</MD5> <!-- auto:datapacks.zip#' mineventurous.xml
 
diff mineventurous.xml.bak mineventurous.xml
